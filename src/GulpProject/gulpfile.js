'use strict';

var gulp = require("gulp"),
    uglify = require("gulp-uglify"),
    rimraf = require("rimraf"),
    concat = require("gulp-concat"),
    sass = require("gulp-sass"),
    merge = require("merge-stream"),
    jscs = require("gulp-jscs"),
    jshint = require("gulp-jshint"),
    plumber = require("gulp-plumber"),
    csslint = require("gulp-csslint"),
    scsslint = require("gulp-scss-lint"),
    gulpif = require("gulp-if"),
    rename = require("gulp-rename"),
    sourcemaps = require("gulp-sourcemaps"),
    autoprefixer = require("gulp-autoprefixer"),
    gutil = require("gulp-util");

// Paths
// TODO: Change these paths to your paths
// You can also add any paths if you need to
var paths = {
    // Source Directory Paths
    bower: "./bower_components/",
    devJs: "./dev/js/",
    devSass: "./dev/sass/",

    // Destination Directory Paths
    css: "./public/css/",
    fonts: "./public/fonts/",
    img: "./public/imgs/",
    js: "./public/js/"
};

// Initialize the mappings between the source and output files.
// TODO: Change these sources to your own sources
var sources = {
    // An array containing objects required to build a single CSS file.
    css: [
        {
            // name - The name of the final CSS file to build.
            name: "font-awesome.css",
            // paths - An array of paths to CSS or SASS files which will be compiled to CSS, concatenated and minified
            // to create a file with the above file name.
            paths: [
                paths.bower + "font-awesome/scss/font-awesome.scss"
            ]
        },
        {
            name: "app.css",
            paths: [
                paths.devSass + "app.scss"
            ]
        },
        {
            name: "bootstrap.css",
            paths: [
                paths.bower + "bootstrap/dist/css/bootstrap.css",
                paths.bower + "bootstrap/dist/css/bootstrap-theme.css"
            ]
        }
    ],
    // An array containing objects required to copy font files.
    fonts: [
        {
            // The name of the folder the fonts will be output to.
            name: "bootstrap",
            // The source directory to get the font files from. Note that we support all font file types.
            path: paths.bower + "bootstrap-sass/**/*.{ttf,svg,woff,woff2,otf,eot}"
        },
        {
            name: "font-awesome",
            path: paths.bower + "font-awesome/**/*.{ttf,svg,woff,woff2,otf,eot}"
        }
    ],
    // An array of paths to images to be optimized.
    img: [
        paths.img + "**/*.{png,jpg,jpeg,gif,svg}"
    ],
    // An array containing objects required to build a single JavaScript file.
    js: [
        {
            // name - The name of the final JavaScript file to build.
            name: "bootstrap.js",
            // paths - A single or array of paths to JavaScript or TypeScript files which will be concatenated and
            // minified to create a file with the above file name.
            paths: [
                // Feel free to remove any parts of Bootstrap you don't use.
                paths.bower + "bootstrap-sass/assets/javascripts/bootstrap/transition.js",
                paths.bower + "bootstrap-sass/assets/javascripts/bootstrap/alert.js",
                paths.bower + "bootstrap-sass/assets/javascripts/bootstrap/button.js",
                paths.bower + "bootstrap-sass/assets/javascripts/bootstrap/carousel.js",
                paths.bower + "bootstrap-sass/assets/javascripts/bootstrap/collapse.js",
                paths.bower + "bootstrap-sass/assets/javascripts/bootstrap/dropdown.js",
                paths.bower + "bootstrap-sass/assets/javascripts/bootstrap/modal.js",
                paths.bower + "bootstrap-sass/assets/javascripts/bootstrap/tooltip.js",
                paths.bower + "bootstrap-sass/assets/javascripts/bootstrap/popover.js",
                paths.bower + "bootstrap-sass/assets/javascripts/bootstrap/scrollspy.js",
                paths.bower + "bootstrap-sass/assets/javascripts/bootstrap/tab.js",
                paths.bower + "bootstrap-sass/assets/javascripts/bootstrap/affix.js"
            ]
        },
        {
            name: "angular.js",
            paths: paths.bower + "angular/angular.js"
        },
        {
            name: "jquery.js",
            paths: paths.bower + "jquery/dist/jquery.js"
        },
        {
            name: "app.js",
            paths: [
                paths.devJs + "app.js"
            ]
        }
    ]
};

/*
 * Deletes all files and folders within the css directory.
 */
gulp.task("clean-css", function(cb) {
    return rimraf(paths.css, cb);
});

/*
 * Deletes all files and folders within the fonts directory.
 */
gulp.task("clean-fonts", function(cb) {
    return rimraf(paths.fonts, cb);
});

/*
 * Deletes all files and folders within the js directory.
 */
gulp.task("clean-js", function(cb) {
    return rimraf(paths.js, cb);
});

/*
 * Deletes all files and folders within the css, fonts and js directories.
 */
gulp.task("clean", ["clean-css", "clean-fonts", "clean-js"]);

/*
 * Report warnings and errors in your CSS and SCSS files (lint them) under the Styles folder.
 */
gulp.task("lint-css", function() {
    return merge([
        // Combine multiple streams to one and return it so the task can be chained.
        gulp.src(paths.devSass + "**/*.{css}") // Start with the source .css files.
        .pipe(plumber()) // Handle any errors.
        .pipe(csslint()) // Get any CSS linting errors.
        .pipe(csslint.reporter()), // Report any CSS linting errors to the console.
        gulp.src(paths.devSass + "**/*.{scss}") // Start with the source .scss files.
        .pipe(plumber()) // Handle any errors.
        .pipe(scsslint()) // Get and report any SCSS linting errors to the console.
    ]);
});

/*
 * Report warnings and errors in your JavaScript files (lint them) under the Scripts folder.
 */
gulp.task("lint-js", function() {
    return merge([
        // Combine multiple streams to one and return it so the task can be chained.
        gulp.src(paths.devJs + "**/*.js") // Start with the source .js files.
        .pipe(plumber()) // Handle any errors.
        .pipe(jshint()) // Get any JavaScript linting errors.
        .pipe(jshint.reporter("default", {
            // Report any JavaScript linting errors to the console.
            verbose: true
        })),
        gulp.src(paths.devJs + "**/*.js") // Start with the source .js files.
        .pipe(plumber()) // Handle any errors.
        .pipe(jscs()) // Get and report any JavaScript style linting errors to the console.
    ]);
});

/*
 * Report warnings and errors in your styles and scripts (lint them).
 */
gulp.task("lint", [
    "lint-css",
    "lint-js"
]);

/*
 * Builds the CSS for the site.
 */
gulp.task("build-css", ["clean-css", "lint-css"], function() {
    var tasks = sources.css.map(function(source) { // For each set of source files in the sources.
        return gulp // Return the stream.
            .src(source.paths) // Start with the source paths.
            .pipe(plumber()) // Handle any errors.
            .pipe(sourcemaps.init()) // Set up the generation of .map source files for the CSS.
            .pipe(gulpif("**/*.scss", sass())) // If the file is a SASS (.scss) file, compile it to CSS (.css).
            .pipe(autoprefixer({
                // Auto-prefix CSS with vendor specific prefixes.
                browsers: [
                    "> 1%", // Support browsers with more than 1% market share.
                    "last 2 versions" // Support the last two versions of browsers.
                ]
            }))
            .pipe(concat(source.name)) // Concatenate CSS files into a single CSS file with the specified name.
            .pipe(sourcemaps.write(".")) // Generates source .map files for the CSS.
            .pipe(gulp.dest(paths.css)); // Saves the CSS file to the specified destination path.
    });
    return merge(tasks); // Combine multiple streams to one and return it so the task can be chained.
});

/*
 * Builds the font files for the site.
 */
gulp.task("build-fonts", ["clean-fonts"], function() {
    var tasks = sources.fonts.map(function(source) { // For each set of source files in the sources.
        return gulp // Return the stream.
            .src(source.path) // Start with the source paths.
            .pipe(plumber()) // Handle any errors.
            .pipe(rename(function(path) { // Rename the path to remove an unnecessary directory.
                path.dirname = "";
            }))
            .pipe(gulp.dest(paths.fonts)); // Saves the font files to the specified destination path.
    });
    return merge(tasks); // Combine multiple streams to one and return it so the task can be chained.
});

/*
 * Builds the JavaScript files for the site.
 */
gulp.task("build-js", [
        "clean-js",
        "lint-js"
    ],
    function() {
        var tasks = sources.js.map(function(source) { // For each set of source files in the sources.
            return gulp // Return the stream.
                .src(source.paths) // Start with the source paths.
                .pipe(plumber()) // Handle any errors.
                .pipe(concat(source.name)) // Concatenate JavaScript files into a single file with the specified name.
                .pipe(sourcemaps.write(".")) // Generates source .map files for the JavaScript.
                .pipe(gulp.dest(paths.js)); // Saves the JavaScript file to the specified destination path.
        });
        return merge(tasks); // Combine multiple streams to one and return it so the task can be chained.
    });

/*
 * Cleans and builds the CSS, Font and JavaScript files for the site.
 */
gulp.task("build", [
    "build-css",
    "build-fonts",
    "build-js"
]);

gulp.task("watch-css", function() {
    return gulp
        .watch(paths.devSass + "**/*.{css,scss}", ["build-css"]) // Watch the styles folder for file changes.
        .on("change", function(event) { // Log the change to the console.
            gutil.log(gutil.colors.blue("File " + event.path + " was " + event.type + ", build-css task started."));
        });
});

/*
 * Watch the dev/js folder for changes to .js or .ts files. Build the JavaScript if something changes.
 */
gulp.task("watch-js", function() {
    return gulp
        .watch(paths.devJs + "**/*.{js,ts}", ["build-js"]) // Watch the scripts folder for file changes.
        .on("change", function(event) { // Log the change to the console.
            gutil.log(gutil.colors.blue("File " + event.path + " was " + event.type + ", build-js task started."));
        });
});

/*
 * Watch the dev/sass, dev/js, and Views folders for changes.
 */
gulp.task("watch", ["watch-css", "watch-js"]);

// default task
gulp.task("default", ["build"]);