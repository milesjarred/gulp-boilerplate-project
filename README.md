# README #

**This is a boilerplate project setup for gulp automation. **

** This boilerplate project is meant for any projects that uses JavaScript for development. **

* Make sure Node.js, npm, and bower are setup on your computer.
* run "npm install"
* run "bower install"
* run "gulp build"

** Start working on your project! **

## Directories ##

* **/app** contains all static files for the main app
* **/dev** Is where all the development is done

## Other Files ##

**gulpfile.js**

* This is where automated tasks are setup using gulp
* Configure this file for each project

**bower.json**

* This is where you get all the dependencies you need such as Angular, Bootstrap, etc...

**package.json**

* This is where all the npm dependencies are setup